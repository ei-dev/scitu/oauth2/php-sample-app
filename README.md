# php-sample-app

TU OAuth2 sample application with PHP

# Require
- PHP >= 7.0
- Composer

# Installation
- git clone https://gitlab.com/ei-dev/scitu/oauth2/php-sample-app
- composer install
- fill application detail in `login.php` file
```redirectUri
    $provider = new \scitu\OAuth2\Client\Provider\TU([
        'clientId'          => '<client id here>',
        'clientSecret'      => '<client secret here>',
        'redirectUri'       => '<redirect url here>',
    ]);
```
> In this sample, redirect uri must register with `http://<your_host>/login.php
- Run server