<?php
    include('vendor/autoload.php');
    session_start();

    $provider = new \scitu\OAuth2\Client\Provider\TU([
        'clientId'          => '<client id here>',
        'clientSecret'      => '<client secret here>',
        'redirectUri'       => '<redirect url here>',
    ]);

    if (!isset($_GET['code'])) {

        // Fetch the authorization URL from the provider; this returns the
        // urlAuthorize option and generates and applies any necessary parameters
        $authorizationUrl = $provider->getAuthorizationUrl();
    
        // Get the state generated for you and store it to the session.
        $_SESSION['oauth2state'] = $provider->getState();
    
        // Redirect the user to the authorization URL.
        header('Location: ' . $authorizationUrl);
        exit;
    
    // Check given state against previously stored one to mitigate CSRF attack
    } elseif (empty($_GET['state']) || (isset($_SESSION['oauth2state']) && $_GET['state'] !== $_SESSION['oauth2state'])) {
    
        if (isset($_SESSION['oauth2state'])) {
            unset($_SESSION['oauth2state']);
        }
        
        exit('Invalid state');
    
    } else {
    
        try {
    
            // Try to get an access token using the authorization code grant.
            $accessToken = $provider->getAccessToken('authorization_code', [
                'code' => $_GET['code']
            ]);
    
            // Using the access token, we may look up details about the
            // resource owner.
            $resourceOwner = $provider->getResourceOwner($accessToken);
    
        } catch (\League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {
    
            // Failed to get the access token or user details.
            echo $e->getMessage();
            exit($e->getMessage());
    
        }

        $_SESSION['accessToken'] = $accessToken;
        $_SESSION['resourceOwner'] = $resourceOwner;
        header('Location: /index.php', true, $permanent ? 301 : 302);
        exit();
    }
?>