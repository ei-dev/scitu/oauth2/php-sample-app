<?php
    include('vendor/autoload.php');
    session_start();
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>TU Oauth2 Sample app</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
</head>
<body>
    <?php if($_SESSION['resourceOwner'] == NULL) : ?>
        <a href='   /login.php'> LOGIN WITH TU </a>
    <?php else : 
        $resourceOwner = $_SESSION['resourceOwner'];
        echo "<br>";
        echo "Hello " . $resourceOwner->getId() ;
        echo "<br>";
        echo "==============================";
        echo "<br>";
        foreach ($resourceOwner->toArray() as $key => $value) {
            echo "" . $key . ": " .$value . "<br>";
        } 
        echo "==============================";
    ?>
    <?php endif; ?>
</body>
</html>